package ictgradschool.industry.lab07.ex06;

import ictgradschool.Keyboard;
import ictgradschool.industry.lab07.ex05.ExerciseFive;

/**
 * TODO Write a program according to the Exercise Six guidelines in your lab handout.
 */
public class ExerciseSix {

    public void start() {
        try {
            System.out.println(userInput());
        } catch (ExceedMaxStringLengthException e) {
            System.out.println("word is too long");
        } catch (InvalidWordException e) {
            System.out.println("Invalid word");
        }
        // TODO Write the codes :)
    }

    // TODO Write some methods to help you.
    public String userInput() throws ExceedMaxStringLengthException, InvalidWordException {
        String userInput;

        System.out.println("Please enter a series of words, you have a maximum of 100 characters");

        userInput = Keyboard.readInput();
        String finalString = userInput.charAt(0) + " ";
        if (userInput.length() > 100) {
            throw new ExceedMaxStringLengthException();
        }
        for (int i = 0; i < userInput.length(); i++) {
            String b = "" + userInput.charAt(0);
            if ("1234567890".contains(b)) {
                throw new InvalidWordException();
            }


            if (userInput.charAt(i) == ' ') {
                String a = "" + userInput.charAt(i + 1);
                if ("1234567890".contains(a)) {
                    throw new InvalidWordException();
                } else {
                    finalString += a.trim() + " ";
                }
            }
        }
        return finalString.trim();
    }


    public class InvalidWordException extends Exception {
    }

    public class ExceedMaxStringLengthException extends Exception {
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        new ExerciseSix().start();
    }
}
