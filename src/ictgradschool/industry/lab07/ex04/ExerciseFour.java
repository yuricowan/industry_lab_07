package ictgradschool.industry.lab07.ex04;

import ictgradschool.Keyboard;

import java.security.Key;

public class ExerciseFour {
    /**
     * Main program entry point. Do not edit, except for uncommenting the marked lines when required.
     */
    public static void main(String[] args) {
        ExerciseFour program = new ExerciseFour();

        // Exercise four, Question 1
           program.divideNumbers();


        // Exercise four, Question 2
        // TODO You may uncomment this line to help test your solution to question two.
       // program.question2();

        // Exercise four, Question 3
        // TODO You may uncomment this line to help test your solution to question three.
       //  program.question3();
    }

    /**
     * The following tries to divide using two user input numbers, but is prone to error.
     * <p>
     * TODO Add some error handling to the code as follows:
     * TODO 1) If the user enters 0 for the second number, "Divide by 0!" should be printed instead of crashing the program.
     * TODO 2) If the user enters numbers which aren't integers, "Input error!" should be printed instead of crashing the program.
     * TODO Both these exceptional cases should be handled in the same try-catch block.
     */
    public void divideNumbers() {

        try {
            System.out.println("Please enter your first number");
            int a = Integer.parseInt(Keyboard.readInput());
            System.out.println("Please enter your second number");
            int b = Integer.parseInt(Keyboard.readInput());
            int c = a / b;
            System.out.println(a + " / " + b + " = " + c);
        } catch (ArithmeticException e) {
            System.out.println("You cannot divide by zero");
        } catch (NumberFormatException e) {
            System.out.println("Invalid input, Please enter a number");
        }
    }

    public void question2() {
        //TODO Write some Java code which throws a StringIndexOutOfBoundsException
        try {
            System.out.println("please enter a phrase");
            String a = Keyboard.readInput();
            System.out.println("Enter an index");
            int b = Integer.parseInt(Keyboard.readInput());
            System.out.println(a.charAt(b));
        } catch (StringIndexOutOfBoundsException e) {
            System.out.println("Exception message: " + e.getMessage());
        }


    }

    public void question3() {
        //TODO Write some Java code which throws a ArrayIndexOutOfBoundsException

        try {
            int[] myArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            System.out.println("Please enter a number which is inside the array");
            int userInput = Integer.parseInt(Keyboard.readInput());
            System.out.println(myArray[userInput]);

        }
        catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Your number is outside the array");
        }
    }
}