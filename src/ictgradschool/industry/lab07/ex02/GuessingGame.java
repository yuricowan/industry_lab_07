package ictgradschool.industry.lab07.ex02;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    /**
     * Plays the actual guessing game.
     * <p>
     * You shouldn't need to edit this method for this exercise.
     */
    public void start() {

        int number = getRandomValue();
        int guess = 0;

        while (guess != number) {

            guess = getUserGuess();

            if (guess > number) {
                System.out.println("Too high!");
            } else if (guess < number) {
                System.out.println("Too low!");
            } else {
                System.out.println("Perfect!");
            }

        }

    }

    /**
     * Gets a random integer between 1 and 100.
     * <p>
     * You shouldn't need to edit this method for this exercise.
     */
    private int getRandomValue() {
        return (int) (Math.random() * 100) + 1;
    }

    /**
     * Gets the user's guess from the keyboard. Currently assumes that the user will always enter a valid guess.
     * <p>
     * TODO Implement some error handling, for the cases where the user enters a value that's too big, too small, or
     * <p>
     * TODO not an integer. Change this method so it's guaranteed to return an integer between 1 & 100, inclusive.
     */

    private int getUserGuess() {
        System.out.print("Enter your guess: ");
        int userGuess = 0;
        try {
            userGuess = Integer.parseInt(Keyboard.readInput());
            if (userGuess < 100 && userGuess > 0) {
                return userGuess;
            } else {
                throw new throwException();
            }
        } catch (NumberFormatException e) {
            System.out.println("Your input is not an integer, please try again");
        } catch (throwException e) {
            System.out.println("Your integer is not within the range. ");
        }
        return userGuess;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
